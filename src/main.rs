mod ast;
mod lex;

use logos::Logos;

use lalrpop_util::lalrpop_mod;
lalrpop_mod!(pub grammar);

fn main() {
    let input = r#"
        using "hello.world";
        using "common.h";

        [attr1][attr2]
        [attr3]
        component TestComp1 {

        }

        enum CameraShakeProceduralBehavior {
            Restartable,
            Additive,
            Exclusive
        }

        component PropSceneActor {
            [ctor][ssl_type("SceneActor")]
            sslOBJ<propSCENE_ACTOR> sceneActor;
            
            [editor]
            bool needFullUpdateOnWriteTransform = false;
        }

        [system_component]
        component DestroyActorOnEntityDestroy {
           [ctor] sslOBJ<iaIACTOR> actor;
        }

        [schedule_worker_thread][iterate_parallel]
        system KillGoreComponents {
           [exclude] PhysSystemKeepAlive _;
           [exclude] PhysSystemSpawnRequest _;
           [include] GoreInfo _;
           [write][optional] AnimInstCreateRequest animRequest;
        }

        message AngularImpulseRequest {
            [ctor] m3dV velocity = m3dVZero;
        }

        // simple comment
        [abstract] [editor("categorizedSection")]
        struct CameraShakeProcedural {
            [editor] [ssl_enum("EcsCameraShakeProceduralBehavior")]
            ecgsCAMERA_SHAKE_PROCEDURAL_BEHAVIOR behaviour;

            [editor][ssl_type("FunctionMotionClip")]
            sslOBJ_REF fovShake;

            [editor][ssl_type("FunctionMotionClip")]
            [category("Offsets")][caption("Offset X")]
            sslOBJ_REF offsetX;

            [editor][ssl_type("FunctionMotionClip")]
            [category("Offsets")][caption("Offset Y")]
            sslOBJ_REF offsetY;

            [editor][ssl_type("FunctionMotionClip")]
            [category("Offsets")][caption("Offset Z")]
            sslOBJ_REF offsetZ;

            [editor][ssl_type("FunctionMotionClip")]
            [category("Rotation")][caption("Rotation angle X")]
            sslOBJ_REF xAngle;

            [editor][ssl_type("FunctionMotionClip")]
            [category("Rotation")][caption("Rotation angle Y")]
            sslOBJ_REF yAngle;

            [editor][ssl_type("FunctionMotionClip")]
            [category("Rotation")][caption("Rotation angle Z")]
            sslOBJ_REF zAngle;

            [editor] float scale;
            [editor] bool isUnique;
            [editor] bool isCycled;

            [editor][units("s")]
            float duration;

            [editor][category("Fade In")]
            [ssl_type("BasicFunctionPreset")]
            sslOBJ_REF fadeIn;

            [editor][category("Fade In")]
            float fadeInDuration;

            [editor][category("Fade Out")]
            [ssl_type("BasicFunctionPreset")]
            sslOBJ_REF fadeOut;

            [editor][category("Fade Out")]
            float fadeOutDuration;

            [editor][ssl_type("LinearRushing1d")]
            sslOBJ<mathLINEAR_RUSHING1D> extMultiplierRusher;

            float externalMultiplier = 1.0;
        }
    "#;

    let lexer = lex::Token::lexer(input);
    let parser = grammar::ContentParser::new();
    let tokens = lexer
        .spanned()
        .map(|(token, span)| (span.start, token, span.end));
    let result = parser.parse(input, tokens);

    println!("{:#?}", result);
}
