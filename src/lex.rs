use logos::Logos;

#[derive(Logos, Debug, PartialEq, Clone)]
pub enum Token<'input> {
    #[token("using")]
    Using,

    #[token("component")]
    Component,

    #[token("system")]
    System,

    #[token("message")]
    Message,

    #[token("struct")]
    Struct,

    #[token("enum")]
    Enum,

    #[token(";")]
    Semicolon,

    #[token("[")]
    LBracket,

    #[token("]")]
    RBracket,

    #[token("{")]
    LBrace,

    #[token("}")]
    RBrace,

    #[token("(")]
    LParen,

    #[token(")")]
    RParen,

    #[token("<")]
    LChevron,

    #[token(">")]
    RChevron,

    #[token("=")]
    Assign,

    #[token(",")]
    Comma,

    #[regex(r"[a-zA-Z_][a-zA-Z0-9_]*")]
    Id(&'input str),

    #[regex(r#""[^"\\]*(?:\\.[^"\\]*)*""#)]
    Str(&'input str),

    #[regex(r"[+-]?[0-9]+")]
    Int(&'input str),

    #[regex(r"[+-]?[0-9]+[.][0-9]+")]
    Float(&'input str),

    #[regex("//[^\n]*", logos::skip)]
    LineComment,

    #[regex(r"/\*([^*]|\*[^/])*\*/", logos::skip)]
    BlockComment,

    #[regex(r"[ \t\r\n]+", logos::skip)]
    #[error]
    Error,
}
