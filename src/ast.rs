use std::fmt;

#[derive(Debug)]
pub struct Block {
    pub attrs: Vec<Attribute>,
    pub content: Box<BlockContent>,
}

#[derive(Debug)]
pub struct BlockContent {
    pub name: String,
    pub items: BlockItems,
}

#[derive(Debug)]
pub enum BlockItems {
    Enum(Vec<String>),
    Component(Vec<Variable>),
    System(Vec<SystemArg>),
    Message(Vec<Variable>),
    Struct(Vec<Variable>),
}

#[derive(Debug)]
pub struct SystemArg {
    pub attrs: Vec<Attribute>,
    pub ty: String,
    pub name: String,
}

#[derive(Debug)]
pub struct Variable {
    pub attrs: Vec<Attribute>,
    pub ty: Type,
    pub name: String,
    pub value: Option<String>,
}

#[derive(Debug)]
pub struct Attribute {
    pub value: String,
    pub arg: Option<String>,
}

impl fmt::Display for Attribute {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.arg {
            None => write!(f, "[{}]", self.value),
            Some(arg) => write!(f, "[{}(\"{}\")]", self.value, arg),
        }
    }
}

#[derive(Debug)]
pub struct Using {
    pub path: String,
}

#[derive(Debug)]
pub struct Content {
    pub usings: Vec<Using>,
    pub blocks: Vec<Block>,
}

#[derive(Debug)]
pub struct Type {
    pub main: String,
    pub subtypes: Vec<Type>,
}

impl fmt::Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.subtypes.is_empty() {
            write!(f, "{}", self.main)
        } else {
            let subtypes = self
                .subtypes
                .iter()
                .map(|ty| ty.to_string())
                .collect::<Vec<_>>()
                .join(", ");
            write!(f, "{}<{}>", self.main, subtypes)
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::ast::*;

    #[test]
    fn test_display_type() {
        assert_eq!(
            Type {
                main: "MAIN".to_string(),
                subtypes: vec![
                    Type {
                        main: "K".to_string(),
                        subtypes: vec![],
                    },
                    Type {
                        main: "V".to_string(),
                        subtypes: vec![],
                    }
                ]
            }
            .to_string(),
            "MAIN<K, V>"
        )
    }

    #[test]
    fn test_display_attribute() {
        assert_eq!(
            Attribute {
                value: "test1".to_string(),
                arg: None,
            }
            .to_string(),
            "[test1]"
        )
    }

    #[test]
    fn test_display_attribute_with_arg() {
        assert_eq!(
            Attribute {
                value: "test1".to_string(),
                arg: Some("test2".to_string()),
            }
            .to_string(),
            "[test1(\"test2\")]"
        )
    }
}
